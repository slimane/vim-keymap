let s:save_cpoptions = &cpoptions
set cpoptions&vim


if exists('g:loaded_keyMap')
    finish
endif

let s:mapCommandDict = {
        \     'n' : {
        \       1 : 'nmap'
        \       , 0 : 'nnoremap'  }
        \   , 'vs' : {
        \       1 : 'vmap'
        \       , 0 : 'vnoremap'  }
        \   , 'o' : {
        \       1 : 'omap'
        \       , 0 : 'onoremap'  }
        \   , 'i' : {
        \       1 : 'imap'
        \       , 0 : 'inoremap'   }
        \   , 'c' : {
        \       1 : 'cmap'
        \       , 0 : 'coremap'   }
        \   , 's' : {
        \       1 : 'smap'
        \       , 0 : 'soremap'   }
        \   , 'x' : {
        \       1 : 'xmap'
        \       , 0 : 'xoremap'   }
        \   , 'nvos' : {
        \               1   : 'map'
        \               , 0 : 'noremap'
        \               }
        \   , 'ic'   : {
        \               1   : 'map!'
        \               , 0 : 'noremap!'
        \               }
        \   , 'icl'  : {
        \               1   : 'lmap'
        \               , 0   : 'lnoremap'
        \               }
        \ }

let s:commandModeDict = {
        \         'noremap'  : 'nvos'
        \       , 'map'      : 'nvos'
        \       , 'nnoremap' : 'n'
        \       , 'nmap'     : 'n'
        \       , 'vnoremap' : 'vs'
        \       , 'vmap'     : 'vs'
        \       , 'onoremap' : 'o'
        \       , 'omap'     : 'o'
        \       , 'noremap!' : 'ic'
        \       , 'map!'     : 'ic'
        \       , 'inoremap' : 'i'
        \       , 'imap'     : 'i'
        \       , 'cnoremap' : 'c'
        \       , 'cmap'     : 'c'
        \       , 'snoremap' : 's'
        \       , 'smap'     : 's'
        \       , 'xnoremap' : 'x'
        \       , 'xmap'     : 'x'
        \       , 'lnoremap' : 'icl'
        \       , 'lmap'     : 'icl'
        \ }





function! keymap#Object#New(key, mode)

    let object = {}

    let object.lhs = ""
    let object.rhs = ""
    let object.silent  = ""
    let object.noremap = ""
    let object.expr = ""
    let object.buffer = ""
    let object.mode = ""
    let object.sid = ""



    " constract
    function! object.New(key, mode) dict
        let l:maparg        = maparg(a:key, a:mode, "", 1)
            return
        endif


        let self.lhs        = l:maparg.lhs
        let self.rhs        = l:maparg.rhs
        let self.noremap    = l:maparg.noremap
        let self.silent     = l:maparg.silent
        let self.expr       = l:maparg.expr
        let self.buffer     = l:maparg.buffer
        let self.mode       =   l:maparg.mode ==? ''  ? 'nvo'
        \                     : l:maparg.mode ==? '!' ? 'ic'
        \                     : l:maparg.mode
        let self.sid        = l:maparg.sid
    endfunction




    function! object.getMode(lhs)
        return get(s:commandModeDict , a:lhs)
    endfunction



    function! object.getMapCommand() dict
        let l:command = get(s:mapCommandDict, self.mode)
        return get(l:command, self.noremap)
    endfunction




    function! object.getExprStr() dict
        return self.expr == 1 ? '<expr>' : ''
    endfunction




    function! object.getBufferStr() dict
        return self.buffer == 1 ? '<buffer>' : ''
    endfunction




    function! object.getSidStr() dict
        return self.sid !=? '' ? '<SNR>' . self.sid . '_' : ''
    endfunction




    function! object.getSilentStr() dict
        return self.silent ==? 1 ? '<silent>' : ''
    endfunction




    function! object.createMap() dict
        if self.rhs == ''
            return ''
        endif

        return
            \     self.getMapCommand()
            \   . " "
            \   . self.getExprStr()
            \   . self.getBufferStr()
            \   . self.getSidStr()
            \   . self.getSidStr()
            \   . self.lhs
            \   . " "
            \   . self.rhs
    endfunction

    call object.New(a:key, object.getMode(a:mode))

    return object
endfunction

let g:loaded_keyMap = 1
let &cpoptions = s:save_cpoptions
